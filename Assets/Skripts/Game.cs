using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    [SerializeField] private int startLives = 3;
    [SerializeField] private string mainMenuName;
    [SerializeField] private GameObject restartMenu;
    [SerializeField] private Text finalCoinCount;
    private int coinCount;
    private int livesCount;

    private void Awake()
    {
        Game[] anotherGame = FindObjectsOfType<Game>();

        if (anotherGame.Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        livesCount = startLives;
    }


    public void RestartGame()
    {
        coinCount = 0;
        livesCount = startLives;
        PlayerUI.ui.SetLives(livesCount);
        restartMenu.SetActive(false);
        Time.timeScale = 1f;
        SceneManager.LoadScene(mainMenuName);
    }
    

    private void GameOver()
    {
        coinCount = 0;
        Time.timeScale = 0f;
        restartMenu.SetActive(true);
        finalCoinCount.text = coinCount.ToString();
    }


    public void  LoseLife()
    {
        livesCount--;
        PlayerUI.ui.SetLives(livesCount);
        if (livesCount <= 0) GameOver();
    }

    public void AddCoins(int amout)
    {
        coinCount += amout;
        PlayerUI.ui.ShowCoinCount(coinCount);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
