using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemy : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private string grounLayerName = "Ground";
    private Rigidbody2D enemyRigidbody2D;
    private bool isFacingRight;

    private void Awake()
    {
        enemyRigidbody2D = GetComponent<Rigidbody2D>();
    }


    private void FixedUpdate()
    {
        Vector2 velocity = new Vector2(isFacingRight ? speed : -speed, enemyRigidbody2D.velocity.y);
        enemyRigidbody2D.velocity = velocity;
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(grounLayerName)) return;

        Flip();
    }


    private void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.Rotate(0f, 180f, 0f);
    }
}
