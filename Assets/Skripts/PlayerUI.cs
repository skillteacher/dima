using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public static PlayerUI ui; 
    [SerializeField] private Text livesText;
    [SerializeField] private Text coinText;

    private void Awake()
    {
        if(ui == null)
        {
            ui = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void SetLives(int amout)
    {
        livesText.text = amout.ToString();
    }

    public void ShowCoinCount(int amout)
    {
        coinText.text = amout.ToString();
    }
}
